import os
from datetime import datetime
import json
import psutil
import platform

from typing import Dict, Union


def list_files(path: str, file_name: str = 'files.json') -> None:
    files = {}
    for dirpath, _, filenames in os.walk(path):
        for filename in filenames:
            full_path = os.path.join(dirpath, filename)
            filename_properties = os.stat(full_path)
            files[full_path] = {
                'name': filename,
                'extension': os.path.splitext(filename)[1],
                'path': dirpath,
                'creation': datetime.fromtimestamp(filename_properties.st_ctime).strftime("%a %b %d %H:%M:%S %Y"),
                'size': filename_properties.st_size
            }
    write_data(data=files, file_name=file_name)


def process_info(process_name: str, file_name: str = 'process_info.txt') -> None:
    process_info = ()
    for proc in psutil.process_iter(['cmdline']):
        if process_name.lower() in proc.name().lower():
            process_info = (proc.name(), proc.pid, proc.cmdline()[0], proc.cmdline()[1:])
    write_data(data=process_info, file_name=file_name)


def os_info(file_name: str = 'os_info.json') -> None:
    platform_info = {
        'os_name': platform.system(),
        'processor': platform.processor(),
        'version': platform.version(),
        'release': platform.release(),
        'architecture': platform.architecture(),
    }
    write_data(data=platform_info, file_name=file_name)


def write_data(data: Union[tuple, Dict[str, Dict[str, str]]], file_name: str) -> None:
    with open(file_name, 'w') as f:
        if os.path.splitext(file_name)[1] == '.json':
            json.dump(data, f)
        elif os.path.splitext(file_name)[1] == '.txt':
            f.write(f"({','.join([str(x) for x in data])})")


if __name__ == '__main__':
    # Task 2
    list_files('C:\Program Files\Wireshark')
    # Task 3
    process_info('Wireshark')
    # Task 4
    os_info()
