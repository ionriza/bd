import socket
from threading import Thread
from time import time, sleep
import re

HOSTNAME = socket.gethostname()
SITES = {}
TOP10_FILENAME = 'top10.txt'
TCP_IP = '127.0.0.1'
TCP_PORT = 5050


class ClientThread(Thread):

    def __init__(self, conn):
        Thread.__init__(self)
        self.conn = conn

    def run(self):
        data = self.conn.recv(2048).decode()
        if 'SESSIONID' not in data:
            self.conn.send('You have to specify a SESSIONID'.encode())
            return
        m = re.match('http:\/\/(\w.+)\?.*SESSIONID=(\w+)', data)
        if not m:
            self.conn.send('Either the protocol is not specified or the sessionid value'.encode())
            return
        site, sessionid = m.groups()
        current_time = time()
        if SITES.get(site):
            if not SITES.get(site).get(sessionid):
                SITES[site].update({sessionid: current_time})
        else:
            SITES[site] = {sessionid: time()}
        all_count = len(SITES[site])
        last_day_count = len(
            [ts for url, sessions in SITES.items() for session, ts in sessions.items() if
             current_time >= ts >= current_time - 60])
        send_data = "\n".join([f'SERVER: {HOSTNAME}', f'URL: {site}', f'ACCESARI ULTIMA ZI: {last_day_count}',
                               f'ACCESARI DINTOTDEAUNA: {all_count}']).encode()
        self.conn.send(send_data)


def show_every_10s():
    start_time = time()
    while True:
        sleep(1)
        if time() > start_time + 60:
            write_top10()
            start_time = time()


def write_top10():
    with open('top10.txt', 'a+') as f:
        f.write('\nSITE\tCOUNT\n')
        for site, sessions in sorted(SITES.items(), key=lambda k: -len(k[1]))[:10]:
            f.write(f'{site}\t{len(sessions)}\n')
        f.write('-' * 16)


timer = Thread(target=show_every_10s, args=())
timer.start()

tcpServer = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcpServer.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
tcpServer.bind((TCP_IP, TCP_PORT))
threads = []

while True:
    tcpServer.listen(4)
    (conn, (ip, port)) = tcpServer.accept()
    newthread = ClientThread(conn)
    newthread.start()
    threads.append(newthread)

for t in threads:
    t.join()
timer.join()
