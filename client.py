import socket
import sys

host = '127.0.0.1'
port = 5050

if len(sys.argv) < 2:
    sys.exit('You have to specify the URL')

site_name = sys.argv[1].encode()

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((host, port))
    s.send(site_name)
    data = s.recv(2048)
    print(data.decode())
