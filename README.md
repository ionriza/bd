# BD



## Getting started

You need the `psutil` library in order to run the `files_processes_os.py`
```
pip install -r requirements.txt
```

## files_processes_os.py

This is the script used in task #2, #3 and #4

All the methods are run in the main method, so you can run the whole script
```
python files_processes_os.py
```

## Client - Server URL tracker

You first start the server
```
python server.py
```
Then start how many clients you need with
```
python client.py http://www.example.com?SESSIONID=123abc
```
You need to specify a URL as an argument in the command line or the script won't work (and also the http:// and SESSIONID)

